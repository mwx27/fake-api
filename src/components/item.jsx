import React, { useState, useEffect } from "react";
import axios from "axios";
import './style.css'
import Modal from 'react-modal';

Modal.setAppElement('#root')

const customStyles = {
  content:{
    top:'50%',left:'50%',
    right:'auto',bottom:'auto',
    transform:'translate(-50%,-50%)',
    marginRight:'-50%',color:'orange',},
  overlay:{backgroundColor:'rgba(90,98,107,.9)'},
};

export default function RenderItems(){
    const [modalIsOpen, setModalIsOpen] = useState(false)
    const [miniModalIsOpen, setMiniModalIsOpen] = useState(false)
    const [modalDescription, setModalDescription] = useState('test')
    const [products, setProducts] = useState()
    const [productName, setProductName] = useState('example product name')
    const [productDescription, setProductDescription] = useState('example product description')
    const [productPrice, setProductPrice] = useState('')
    const [productImage, setProductImage] = useState('')
    const [product1Clicked, setProduct1Clicked] = useState(false)
    const [product2Clicked, setProduct2Clicked] = useState(false)
    const [product3Clicked, setProduct3Clicked] = useState(false)
    const baseURL = "https://fakestoreapi.com/products"

    useEffect(() => {
      axios.get(baseURL)
      .then(resp => {console.log(baseURL); console.log(resp.data); setProducts(resp.data); })
      .catch(err => alert(err.message))
    }, []);

    function unClickAll(){
      setProduct1Clicked(false)
      setProduct2Clicked(false)
      setProduct3Clicked(false)}

    function imgClickHandle(e){
      if(e.target.alt==='product1'){
        unClickAll()
        setProduct1Clicked(true)
        setProductName('Bentley')
        setProductDescription('Very luxury car')
        setProductPrice(5000000)
      } else if(e.target.alt==='product2'){
        unClickAll()
        setProduct2Clicked(true)
        setProductName('Peanuts')
        setProductDescription('Healthy for your brain')
        setProductPrice(5)
      } else if(e.target.alt==='product3'){
        unClickAll()
        setProduct3Clicked(true)
        setProductName('Pork brains')
        setProductDescription('Tasty and nutritious')
        setProductPrice(15)
      }
      setProductImage(e.target.src)
      }
    function addProduct(name, descr, price, img){
      const newProduct={title: name,price: price,description: descr,image: img}
      axios.post('https://fakestoreapi.com/products', newProduct)
        .then(res => {setProducts([...products,res.data])})
        .catch(error => console.log(error))
    }
      
    return(
      <div>
        <button onClick={() => {setModalIsOpen(true)}}>Add products</button>

        <Modal isOpen={modalIsOpen} onRequestClose={() => {setModalIsOpen(false)}} style={customStyles}>

          <h2>Adding new product do database</h2>
          <div><label>Name:<br/><input type="text" style={{width: `${30*7.85}px`}} value={productName} onChange={(e) => {setProductName(e.target.value)}}/></label></div>
          <div><label>Description:<br/><textarea rows={3} cols={30} type="text" value={productDescription} onChange={(e) => {setProductDescription(e.target.value)}}/></label></div>
          <div><label>Price:<br/><input type="text" style={{width: `${30*7.85}px`}} value={productPrice} onChange={(e) => {setProductPrice(e.target.value)}}/></label></div>
          <div>Choose image</div>
          <img 
            src='https://imageshack.com/i/eymZYW7Fj'
            onClick={(e) => {imgClickHandle(e)}} 
            alt='product1'
            className={product1Clicked?'imgAdminClicked':'imgAdmin'}/>
          <img 
            src='https://i.imgur.com/Unr2NVK.jpeg'
            onClick={(e) => {imgClickHandle(e)}} 
            alt='product2'
            className={product2Clicked?'imgAdminClicked':'imgAdmin'}/>          
          <img 
            src='https://i.imgur.com/ztgTGo9.jpeg'
            onClick={(e) => {imgClickHandle(e)}} 
            alt='product3'
            className={product3Clicked?'imgAdminClicked':'imgAdmin'}/>
          
          <div>
            <button onClick={() => {
              addProduct(productName,productDescription,productPrice,productImage)
              setModalDescription(`${productName} added successfully`)
              setMiniModalIsOpen(true);
              }}>Add product</button>
            <button onClick={() => {setModalIsOpen(false)}}>Cancel</button>
          </div>

          <div>
            <p>Name: {productName}</p>
            <p>Description: {productDescription}</p>
            <p>Price: {productPrice} $</p>
            <p>Image: {productImage}</p>
          </div>
        </Modal>
            
        <Modal isOpen={miniModalIsOpen} onRequestClose={() => {setMiniModalIsOpen(false)}} style={customStyles}>
          <div className="item">
            {modalDescription}
            <button style={{marginTop:'15px'}} onClick={() => {setMiniModalIsOpen(false)}}>Ok</button>
          </div>
        </Modal>

        <div className="cont">

          {products ? products.map(item => {return (
          <div className="itemsContainer">
            <div className="item1">{item.title}</div>
            <div className="item2">Price: {item.price} $</div>
            <div className="item3">
              <button value={item.description} onClick={(e) => {
                setModalDescription(e.target.value)
                setMiniModalIsOpen(true);
                console.log(miniModalIsOpen)
              }}>
                Show description
              </button>
              <button value={item.id} onClick={(e) => {
                setModalDescription(`Product with id ${e.target.value} has been added to the cart`)
                setMiniModalIsOpen(true);
              }}>
                Buy me!
              </button>
            </div>
            <div className="item item4">
              <img src={item.image} alt=""/>  
            </div>
          </div>
          )}) : <div className='item4'>"waiting for the books..."</div>}
          
        </div>
  </div>

    )

}